package com.manelcc.t15ej2;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private ImageView imageCompass;
    private float currentDegree = 0f;
    private SensorManager mSensorManager;
    private TextView textViewDegrees;
    private Sensor sensor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageCompass = (ImageView) findViewById(R.id.imageViewCompass);
        textViewDegrees = (TextView) findViewById(R.id.textViewDegrees);
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = mSensorManager.getSensorList(Sensor.TYPE_MAGNETIC_FIELD).get(0);
        mSensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
// TODO Obtén una instancia de SensorManager y
// registra el listener para utilizar la brújula (MAGNETIC_FIELD)
    }

    @Override
    protected void onPause() {
        super.onPause();
//TODO Liberar el listener para no gastar batería.
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
//Obtiene el ángulo de la brújula
        float degree = Math.round(event.values[0]);
        textViewDegrees.setText(Float.toString(degree) + " degrees");
//Crea la animación de la rotación
        RotateAnimation ra = new RotateAnimation(
                currentDegree,
                -degree,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f);
        ra.setDuration(210);
// set the animation after the end of the reservation status
        ra.setFillAfter(true);
// Start the animation
        imageCompass.startAnimation(ra);
        currentDegree = -degree;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }
}

